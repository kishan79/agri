const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FirmSchema = new Schema({
    id:{
        type: Number,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    address:[{
        area:{
            type: String
        },
        landmark:{
            type: String
        },
        city:{
            type: String,
            required: true
        },
        pin:{
            type: Number,
            required: true
        },
        state:{
            type: String,
            required: true
        }
    }],
    contact:{
        type: Number,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    website:{
        type: String
    },
    metadata:{
        type: String
    },
    date:{
        type: Date,
        default: Date.now
    }
});

mongoose.model('firm',FirmSchema);